package core.helpers.legacy;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import core.helpers.Element.DriverType;
import core.support.listeners.TestListener;
import core.support.logger.TestLog;
import core.support.objects.TestObject;
import core.uiCore.drivers.AbstractDriver;
import core.uiCore.webElement.EnhancedBy;

public class DriverLegacy {

	/**
	 * uses WebElement to interact with Autonomx Helpers
	 * This also means using helpers with index is not feasible
	 * @param target
	 * @return
	 */
	
	public static EnhancedBy getEnhancedElement(WebElement target) {
		EnhancedBy element = new EnhancedBy();
		element.LegacyWebElements = new ArrayList<WebElement>();
		element.LegacyWebElements.add(target);
//		element.setName(getLocatorName(target));
		return element;
	}
	
	public static EnhancedBy getEnhancedElement(List<WebElement> targets) {		
		EnhancedBy element = new EnhancedBy();
		
		element.LegacyWebElements = new ArrayList<WebElement>();
		for(WebElement target : targets)
			element.LegacyWebElements.add(target);
		
		if(targets.size()>0)
			element.setName(getLocatorName(targets.get(0)));

		return element;
	}
	
//	public static EnhancedBy getEnhancedElement2(WebElement target) {
//		List<By> locators = getLocatorBy(target);
//		EnhancedBy element = new EnhancedBy();
//		for(By locator : locators) {
//			element.byElement(locator, DriverType.Web);
//		}
//		return element;
//	}
//	
//	public static EnhancedBy getEnhancedElement3(WebElement target) {
//		List<String[]> locators = getLocator(target);
//		EnhancedBy element = setEnhancedElement(locators);
//
//		return element;
//	}

	/**
	 * retrieves locator values from a webElement
	 * 
	 * @param element
	 * @return
	 */
	protected static List<String[]> getLocator(WebElement element) {
		List<String[]> paths = new ArrayList<String[]>();
		try {

			// get the by value string from the web element containing format by:locator
			Object findBy = getByValueFromWebElement(element);
			paths = getLocators(findBy.toString());

		} catch (Exception error) {
			TestLog.ConsoleLogError("element could not be parsed: " + element + " Error: " + error.getMessage());
		}

		if (paths.isEmpty())
			TestLog.ConsoleLogError("element could not be parsed: " + element);
		return paths;
	}
	 
	/**
	 * attempts to get the name of the webElement locator
	 * @param element
	 * @return
	 */
    public static String getLocatorName(WebElement element) {		
		
		Field[] fields = FieldUtils.getAllFields(element.getClass());
		boolean isMobileElement = Arrays.toString(fields).contains("CGLIB$CALLBACK_0");
		String name = "";
		try {
			// Mobile = CGLIB$CALLBACK_0, web = h
			Object proxyOrigin = null;
			if (isMobileElement)
				proxyOrigin = FieldUtils.readField(element, "CGLIB$CALLBACK_0", true);
			else
				proxyOrigin = FieldUtils.readField(element, "h", true);

			Object locator = FieldUtils.readField(proxyOrigin, "locator", true);
			name =  (String) FieldUtils.readField(locator, "name", true);
      
           return name;

		} catch (Exception ignored) {
			return "";
		}
	}
	
	/**
	 * Deprecated
	 * @param element
	 * @return
	 */
     public static List<By> getLocatorBy(WebElement element) {		
		
		Field[] fields = FieldUtils.getAllFields(element.getClass());
		boolean isMobileElement = Arrays.toString(fields).contains("CGLIB$CALLBACK_0");
		By findBy = null;
		By[] findBys = null;
        List<By> byList = new ArrayList<By>();
		try {
			// Mobile = CGLIB$CALLBACK_0, web = h
			Object proxyOrigin = null;
			if (isMobileElement)
				proxyOrigin = FieldUtils.readField(element, "CGLIB$CALLBACK_0", true);
			else
				proxyOrigin = FieldUtils.readField(element, "h", true);

			Object locator = FieldUtils.readField(proxyOrigin, "locator", true);
			findBy = (By) FieldUtils.readField(locator, "by", true);
			findBys = (By[]) FieldUtils.readField(findBy, "bys", true);
            for(By by : findBys) {
            	byList.add(by);
            }
            return byList;

		} catch (Exception ignored) {
			
		}
		byList.add(findBy);
		return byList;
	}

	/**
	 * 	Deprecated

	 * returns the by value string from the web element, containing by value and
	 * locator value as object example: By.all({By.id: email2,By.id: email,By.id:
	 * email3}) or By.chained({By.all({By.all({By.id: email2,By.id: email,By.id:
	 * email3}))}
	 * 
	 * @param element
	 * @return
	 */
	public static Object getByValueFromWebElement(WebElement element) {		
		
		Field[] fields = FieldUtils.getAllFields(element.getClass());
		boolean isMobileElement = Arrays.toString(fields).contains("CGLIB$CALLBACK_0");
		Object findBy = null;
		try {
			// Mobile = CGLIB$CALLBACK_0, web = h
			Object proxyOrigin = null;
			if (isMobileElement)
				proxyOrigin = FieldUtils.readField(element, "CGLIB$CALLBACK_0", true);
			else
				proxyOrigin = FieldUtils.readField(element, "h", true);

			Object locator = FieldUtils.readField(proxyOrigin, "locator", true);
			findBy = FieldUtils.readField(locator, "by", true);

			String modifieidLocator = findBy.toString();
			// to handle by chained locator strategy. Gets first locator value
			if (modifieidLocator.contains("By.chained")) {
				if (findBy.toString().contains("By.chained({By.all({")) {
					modifieidLocator = modifieidLocator.replace("By.chained({By.all({", "");
					modifieidLocator = modifieidLocator.replace("})})", "");
				} else if (findBy.toString().contains("By.chained({")) {
					modifieidLocator = modifieidLocator.replace("By.chained({", "");
					modifieidLocator = modifieidLocator.replace("})", "");
				}
			} else if (modifieidLocator.contains("By.all")) {
				modifieidLocator = modifieidLocator.replace("By.all({", "");
				modifieidLocator = modifieidLocator.replace("})", "");
			}
			findBy = modifieidLocator;

		} catch (Exception ignored) {
			return getLocatorThroughParsing(element.toString());
		}

		return findBy;
	}

	/**
	 * 	 Deprecated

	 * attempt to get the locator from WebElement through parsing the toString()
	 * 
	 * @param element
	 * @return
	 */
	public static String getLocatorThroughParsing(String element) {
		String path = "";
		try {

			path = element;
			
			if(path.contains("Located by"))
				path = path.replace("Located by", "");
			if (path.contains("By.chained")) {
				if (path.contains("By.chained({By.all({")) {
					path = path.replace("By.chained({By.all({", "");
					path = path.replace("})})", "");
				} else if (path.contains("By.chained({")) {
					path = path.replace("By.chained({", "");
					path = path.replace("})", "");
				}
			} else if (path.contains("By.all")) {
				path = path.replace("By.all({", "");
				path = path.replace("})", "");
			}
			return path;
		} catch (Exception e) {
			TestLog.ConsoleLogError("Element :( " + element + " ) not containing valid by:locator format!");
			return path;
		}
	}
	
	/**
	 * 	Deprecated

	 * @param element
	 * @return
	 */
	public static List<String[]> getLocatorThroughParsing2(String element) {
		String[] locator = new String[2];
		List<String[]> paths = new ArrayList<String[]>();

		try {

			String path = element;
			String[] pathVariables = null;
			if (path.contains("->"))
				pathVariables = (path.split("->", 2)[1].replaceFirst("(?s)(.*)\\]", "$1" + "")).trim().split(":", 2);
			else {
				pathVariables = path.split("'", 2)[1].replaceAll("'$", "").trim().split(":", 2);
				pathVariables[0] = pathVariables[0].split("By.")[1];
			}

			if (pathVariables.length != 2)
				throw new IllegalStateException("webElement :( " + path + " )not containg valid by:locator format!");

			String selector = pathVariables[0].trim();
			String value = pathVariables[1].trim();

			locator[0] = selector;
			locator[1] = value;

			paths.add(locator);

			return paths;
		} catch (Exception e) {
			TestLog.ConsoleLogError("webElement :( " + element.toString() + " )not containg valid by:locator format!");
			return paths;
		}
	}

	/**
	 * 	 Deprecated

	 * eg. "By.xpath: //android.widget.Button[contains(@text,,
	 * 'Continue')],AppiumBy.id: com.telus.mywifi:id/button_continue"
	 * 
	 * @param element
	 * @return
	 */
	public static List<String[]> getLocators(String element) {
		
		// incase of remote web element
		if(element.contains("->")) {
			element = element.split("->", 2)[1];
		}
		
        // get locator split by ","
		List<String> locatorList = new ArrayList<String>();
		String currentLocator = "";
		String[] locators = element.split(",");
		for (int i = 0; i < locators.length; i++) {
			if (!locators[i].contains("By") && i > 0) {
				currentLocator = currentLocator + ", " + locators[i];
				locatorList.set(locatorList.size() - 1, currentLocator);
			} else {
				locatorList.add(locators[i]);
				currentLocator = locators[i];
			}
		}
		// convert to list of by and location
		String[] locatorArray = locatorList.toArray(String[]::new);
		String[] path = new String[2];
		List<String[]> paths = new ArrayList<String[]>();
		for (String locator : locatorArray) {
			// get by and locator value
			if (locator != null) {
				path = locator.toString().split(":", 2);
				if (path.length != 2) {
					TestLog.ConsoleLog("element could not be parsed: " + locator);
					continue;
				}
				path[0] = path[0].split("By.")[1];
				paths.add(path);
			}
		}

		return paths;
	}

	/**
	 * set the webDriver from webDriver or mobileDriver without relying on global
	 * flag
	 */
	public static void setDriver(WebDriver driver) {
		TestListener.isTestNG = true;
		TestObject.IS_PROPERTIES_DISABLED = true;
		AbstractDriver.TIMEOUT_SECONDS = 60;
		AbstractDriver.TIMEOUT_IMPLICIT_SECONDS = 5;
		AbstractDriver.setWebDriver(driver);
	}

	public static void setDriver(WebDriver driver, boolean isPropertyDisabled, int timeoutSec, int implicitSec) {
		TestListener.isTestNG = true;
		TestObject.IS_PROPERTIES_DISABLED = isPropertyDisabled;
		AbstractDriver.TIMEOUT_SECONDS = timeoutSec;
		AbstractDriver.TIMEOUT_IMPLICIT_SECONDS = implicitSec;
		AbstractDriver.setWebDriver(driver);
	}

	/**
	 * 	 Deprecated
	 * @param locators
	 * @return
	 */
	protected static EnhancedBy setEnhancedElement(List<String[]> locators) {
		EnhancedBy element = new EnhancedBy();

		for (String[] locator : locators) {

			String selector = locator[0].trim();
			String value = locator[1].trim();

			DriverType driverType = null;

			if (Helper.isAndroid())
				driverType = DriverType.Android;
			else if (Helper.isIOS())
				driverType = DriverType.iOS;
			else
				driverType = DriverType.Web;

			switch (selector) {
			case "accessibility":
				element.byAccessibility(value, value, driverType);
				break;
			case "cssSelector":
			case "css selector":
			case "css":
				element.byCss(value, value);
				break;
			case "xpath":
				element.byXpath(value, value, driverType);
				break;
			case "name":
				element.byName(value, value);
				break;
			case "id":
				element.byId(value, value, driverType);
				break;
			case "class":
			case "class name":
			case "className":
				element.byClass(value, value, driverType);
				break;
			case "tag name":
			case "tagName":
				element.byTagName(value, value);
				break;
			case "link text":
			case "linkText":
				element.byLinkText(value, value);
				break;
			case "partial link text":
			case "partialLinkText":
				element.byPartialLinkText(value, value);
				break;
			case "iOSNsPredicate":
				element.byIOSNsPredicateString(value, value, driverType);
				break;
			case "androidUIAutomator":
			case "uiAutomator":
				element.byAndroidUIAutomator(value, value, driverType);
				break;
			case "androidDataMatcher":
				element.byAndroidDataMatcher(value, value, driverType);
				break;
			case "iOSClassChain":
				element.byIOSClassChain(value, value, driverType);
				break;
			default:
				if (element.elementObject.isEmpty())
					Helper.assertFalse("selector: " + selector + " not part of selector types");
				break;
			}
		}
		return element;
	}
}