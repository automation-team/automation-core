package core.uiCore.webElement;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import core.helpers.Element.DriverType;
import core.helpers.Element.LocatorType;
import core.helpers.Helper;
import io.appium.java_client.AppiumBy;

/**
 * Elements are stored in list
 * 
 * @author CAEHMAT
 *
 */
public class EnhancedBy {
	public List<ElementObject> elementObject = new ArrayList<ElementObject>();
	public String name = "";
	public List<WebElement> LegacyWebElements =  new ArrayList<WebElement>(); // handling legacy drivers

	public EnhancedBy() {
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public EnhancedBy byCss(String element, String name) {         
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(By.cssSelector(element), name, element, LocatorType.css, DriverType.Web);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}

	public EnhancedBy byCss(String element) {
		return byCss(element, name);
	}

	public EnhancedBy byXpath(String element, String name, DriverType driverType) {
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(By.xpath(element), name, element, LocatorType.xpath, driverType);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}
	
	public EnhancedBy byXpath(String element, String name) {
		return byXpath(element, name, DriverType.Web);
	}

	public EnhancedBy byXpath(String element) {
		return byXpath(element, name, DriverType.Web);
	}
	
	public EnhancedBy byMobileXpath(String element, String name, DriverType driverType) {
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(AppiumBy.xpath(element), name, element, LocatorType.xpath, driverType);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}
	
	public EnhancedBy byAndroidXpath(String element, String name) {
		return byMobileXpath(element, name, DriverType.Android);
	}
	
	public EnhancedBy byAndroidXpath(String element) {
		return byMobileXpath(element, name, DriverType.Android);
	}
	
	public EnhancedBy byiOSXpath(String element, String name) {
		return byMobileXpath(element, name, DriverType.iOS);

	}
	
	public EnhancedBy byiOSXpath(String element) {
		return byMobileXpath(element, name, DriverType.iOS);
	}

	public EnhancedBy byId(String element, String name, DriverType driverType) {
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(By.id(element), name, element, LocatorType.id, driverType);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}
	
	public EnhancedBy byMobileId(String element, String name, DriverType driverType) {
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(AppiumBy.id(element), name, element, LocatorType.id, driverType);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}

	public EnhancedBy byId(String element, String name) {
		return byId(element, name, DriverType.Web);
	}
	
	public EnhancedBy byId(String element) {
		return byId(element, name, DriverType.Web);
	}
	
	public EnhancedBy byAndroidId(String element, String name) {
		return byMobileId(element, name, DriverType.Android);

	}
	
	public EnhancedBy byAndroidId(String element) {
		return byMobileId(element, name, DriverType.Android);
	}
	
	public EnhancedBy byiOSId(String element, String name) {
		return byMobileId(element, name, DriverType.iOS);

	}
	
	public EnhancedBy byiOSId(String element) {
		return byMobileId(element, name, DriverType.iOS);
	}

	public EnhancedBy byName(String element, String name) {
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(By.name(element), name, element, LocatorType.name, DriverType.Web);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}

	public EnhancedBy byName(String element) {
		return byName(element, name);
	}

	public EnhancedBy byClass(String element, String name, DriverType driverType) {
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(By.className(element), name, element, LocatorType.classType, driverType);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}
	
	public EnhancedBy byMobileClass(String element, String name, DriverType driverType) {
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(AppiumBy.className(element), name, element, LocatorType.classType, driverType);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}
	
	public EnhancedBy byClass(String element, String name) {
		return byClass(element, name, DriverType.Web);
	}

	public EnhancedBy byClass(String element) {
		return byClass(element, name, DriverType.Web);
	}
	
	public EnhancedBy byAndroidClass(String element, String name) {
		return byMobileClass(element, name, DriverType.Android);

	}
	
	public EnhancedBy byAndroidClass(String element) {
		return byMobileClass(element, name, DriverType.Android);
	}
	
	public EnhancedBy byiOSClass(String element, String name) {
		return byMobileClass(element, name, DriverType.iOS);

	}
	
	public EnhancedBy byiOSClass(String element) {
		return byMobileClass(element, name, DriverType.iOS);
	}
	
	public EnhancedBy byTagName(String element, String name) {
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(By.tagName(element), name, element, LocatorType.tagName, DriverType.Web);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}

	public EnhancedBy byTagname(String element) {
		return byClass(element, name);
	}
	
	public EnhancedBy byLinkText(String element, String name) {
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(By.linkText(element), name, element, LocatorType.linkText, DriverType.Web);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}

	public EnhancedBy byLinkText(String element) {
		return byClass(element, name);
	}
	
	public EnhancedBy byPartialLinkText(String element, String name) {
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(By.partialLinkText(element), name, element, LocatorType.partialLinkText, DriverType.Web);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}

	public EnhancedBy byPartialLinkText(String element) {
		return byClass(element, name);
	}

	public EnhancedBy byAccessibility(String element, String name, DriverType driverType) {
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(AppiumBy.accessibilityId(element), name, element,
				LocatorType.accessibiliy, driverType);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}
	
	public EnhancedBy byAccessibility(String element, String name) {
		return byAccessibility(element, name, DriverType.iOS);
	}
	
	
	public EnhancedBy byAccessibility(String element) {
		return byAccessibility(element, name, DriverType.iOS);
	}
	
	public EnhancedBy byiOSAccessibility(String element, String name) {
		return byAccessibility(element, name, DriverType.iOS);
	}
	
	public EnhancedBy byiOSAccessibility(String element) {
		return byAccessibility(element, name, DriverType.iOS);
	}
	
	
	
	public EnhancedBy byAndroidAccessibility(String element, String name) {
		return byAccessibility(element, name, DriverType.Android);
	}
	
	public EnhancedBy byAndroidAccessibility(String element) {
		return byAccessibility(element, name, DriverType.Android);
	}
	
	public EnhancedBy byIOSNsPredicateString(String element, String name, DriverType driverType) {
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(AppiumBy.iOSNsPredicateString(element), name, element, LocatorType.iOSNsPredicateString, driverType);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}
	
	public EnhancedBy byIOSNsPredicateString(String element, String name) {
		return byIOSNsPredicateString(element, name, DriverType.iOS);
	}
	
	public EnhancedBy byIOSNsPredicateString(String element) {
		return byIOSNsPredicateString(element, name, DriverType.iOS);
	}
	
	public EnhancedBy byAndroidUIAutomator(String element, String name, DriverType driverType) {		
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(AppiumBy.androidUIAutomator(element), name, element, LocatorType.androidUIAutomator, driverType);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}
	
	public EnhancedBy byAndroidUIAutomator(String element, String name) {
		return byAndroidUIAutomator(element, name, DriverType.Android);
	}
	
	public EnhancedBy byAndroidUIAutomator(String element) {
		return byAndroidUIAutomator(element, name, DriverType.Android);
	}
	
	public EnhancedBy byIOSClassChain(String element, String name, DriverType driverType) {
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(AppiumBy.iOSClassChain(element), name, element, LocatorType.iOSClassChain, driverType);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}
	
	public EnhancedBy byIOSClassChain(String element, String name) {
		return byIOSClassChain(element, name, DriverType.iOS);

	}
	
	public EnhancedBy byIOSClassChain(String element) {
		return byIOSClassChain(element, name, DriverType.iOS);
	}
	
	public EnhancedBy byAndroidDataMatcher(String element, String name, DriverType driverType) {
		Helper.assertTrue("element cannot be empty", !element.isEmpty());

		ElementObject locatorObject = new ElementObject(AppiumBy.androidDataMatcher(element), name, element, LocatorType.androidDataMatcher, driverType);
		elementObject.add(locatorObject);
		this.name = name;
		return this;
	}
	
	public EnhancedBy byAndroidDataMatcher(String element, String name) {
		return byAndroidDataMatcher(element, name, DriverType.Android);
	}
	
	public EnhancedBy byAndroidDataMatcher(String element) {
		return byAndroidDataMatcher(element, name, DriverType.Android);
	}
	
	public EnhancedBy byElement(By by, DriverType driverType) {

		ElementObject locatorObject = new ElementObject(by, "", "", LocatorType.generic, driverType);
		elementObject.add(locatorObject);
		return this;
	}
}
